package com.deckbuilder.backend.repository;

import com.deckbuilder.backend.model.CardsInDeck;
import org.springframework.data.repository.CrudRepository;

public interface CardsInDeckRepository extends CrudRepository<CardsInDeck, Long> {
}
