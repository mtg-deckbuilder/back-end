package com.deckbuilder.backend.repository;

import com.deckbuilder.backend.model.Deck;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.Id;

public interface DeckRepository extends CrudRepository<Deck, Long> {
}
