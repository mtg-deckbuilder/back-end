package com.deckbuilder.backend.controller;

import com.deckbuilder.backend.model.*;
import com.deckbuilder.backend.repository.*;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class UserController {
    private UserRepository repo;
    public UserController(UserRepository repo){this.repo =repo;}
    @GetMapping("/user")
    public List<User> userList(){

        List<User> list = (List<User>)repo.findAll();
        System.out.println(list.size());
        return list;
    }
    @PostMapping("/user/create")
    public User createItem(@RequestBody User user){
        user.setProfilePicture("../../assets/"+user.getProfilePicture());
        repo.save(user);
        return user;
    }
}
