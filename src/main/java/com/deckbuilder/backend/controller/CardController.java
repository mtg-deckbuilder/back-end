package com.deckbuilder.backend.controller;

import com.deckbuilder.backend.model.*;
import com.deckbuilder.backend.repository.*;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CardController {
    private CardRepository repo;

    public CardController(CardRepository repo) {
        this.repo = repo;
    }

    @GetMapping("/cards")
    public List<Card> cardList() {

        List<Card> list = (List<Card>) repo.findAll();
        System.out.println(list.size());
        return list;
    }

    @PostMapping("/create")
    public Card createItem(@RequestBody Card card) {
        card.setImgsource("../../assets/" + card.getImgsource());
        repo.save(card);
        return card;
    }
    @PostMapping(path = "/inventory")
    public Card ChangeQuantityToServer (@RequestBody Card card){
//        System.out.println("hi");
//        int temp = card.getQuantity();
//        System.out.println(x);
//        card.setQuantity(temp + x);
        repo.save(card);
        return card;

    }
}
