package com.deckbuilder.backend.model;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;

@Entity
@Getter
@Setter
public class Deck {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @ManyToOne
    @JoinColumn(name = "owner", referencedColumnName = "ID")
    private User owner;
}
//    @JsonBackReference
//    @JsonIgnore
