package com.deckbuilder.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.deckbuilder.backend.model.Card;

@Repository
public interface CardRepository extends CrudRepository<Card, Long> {

}
