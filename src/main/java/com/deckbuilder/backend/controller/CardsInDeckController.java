package com.deckbuilder.backend.controller;

import com.deckbuilder.backend.model.CardsInDeck;
import com.deckbuilder.backend.model.Deck;
import com.deckbuilder.backend.repository.CardsInDeckRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CardsInDeckController {
    private CardsInDeckRepository cidRepo;

    public CardsInDeckController(CardsInDeckRepository cidRepo){
        this.cidRepo = cidRepo;
    }
    @GetMapping("/deck/cards")
    public List<CardsInDeck> getCardsInDeck(){
        List<CardsInDeck> list = (List<CardsInDeck>) cidRepo.findAll();
        System.out.println(list.size());
        return list;
    }

    @PostMapping("/deck/cards/add")
    public List<CardsInDeck> createItem(@RequestBody CardsInDeck cardsInDeck){
        cidRepo.save(cardsInDeck);
        return (List<CardsInDeck>) cidRepo.findAll();
    }
}
