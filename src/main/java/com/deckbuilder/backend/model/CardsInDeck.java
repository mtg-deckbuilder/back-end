package com.deckbuilder.backend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class CardsInDeck {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "owner", referencedColumnName = "ID")
    private Deck owner;
    @ManyToOne
    @JoinColumn(name = "cardInstance", referencedColumnName = "ID")
    private Card cardInstance;
}
