package com.deckbuilder.backend.controller;

import com.deckbuilder.backend.model.Deck;
import com.deckbuilder.backend.repository.DeckRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class DeckController {
    private DeckRepository deckRepo;

    public DeckController(DeckRepository deckRepo){
        this.deckRepo = deckRepo;
    }
    @GetMapping("/deck")
    public List<Deck> getDecks(){
        return (List<Deck>) deckRepo.findAll();
    }

    @PostMapping("/deck/add")
    public List<Deck> createItem(@RequestBody Deck deck){
        deckRepo.save(deck);
        return (List<Deck>) deckRepo.findAll();
    }
}
